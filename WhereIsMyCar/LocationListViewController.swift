//
//  LocationListViewController.swift
//  WhereIsMyCar
//
//  Created by Arthur JANSSENS on 17/04/2017.
//  Copyright © 2017 Eleves de Supinfo. All rights reserved.
//

import UIKit

class LocationListViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchDisplayDelegate, UISearchBarDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.reloadData()
        let recognizer = UILongPressGestureRecognizer(target: self, action:#selector(onLongPressCell(recognizer:)))
        tableView.addGestureRecognizer(recognizer)
    }
    
    @IBAction func unwindToListViewController(segue: UIStoryboardSegue){
    
    }

    // MARK: - Table View
    func numberOfSections(in tableView: UITableView) -> Int
    {
        // We only need one column
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        // Number of cells to display
        return UserLocations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        // Building the cell
        
        let cell: LocationCell = self.tableView.dequeueReusableCell(withIdentifier: "SavedLocationCell") as! LocationCell
        let savedLocation = UserLocations[indexPath.row]
        
        cell.titleCell.text = savedLocation.Name
        cell.commentCell.text = savedLocation.Comment
        return cell
        
    }
    
    func askAndRemovePosition(id:Int) {
        let alert = UIAlertController(title: "Remove", message: "Do you want to remove this position ?", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Delete", style: UIAlertActionStyle.default, handler: { action in
            
            let isRemoveSuccessful = UserLocations.remove(at: id)
            
            if isRemoveSuccessful != nil {
                self.tableView.reloadData()
            } else {
                let alert2 = UIAlertController(title: "Error", message: "Failed to remove location.", preferredStyle: UIAlertControllerStyle.alert)
                alert2.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                self.present(alert2, animated: true, completion: nil)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "Don't Delete", style: UIAlertActionStyle.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            askAndRemovePosition(id: indexPath.row)
        }
    }
    
    func onLongPressCell(recognizer: UIGestureRecognizer) {
        if recognizer.state == UIGestureRecognizerState.ended {
            let swipeLocation = recognizer.location(in: self.tableView)
            if let swipedIndexPath = tableView.indexPathForRow(at: swipeLocation) {
                if self.tableView.cellForRow(at: swipedIndexPath) != nil {
                    askAndRemovePosition(id: swipedIndexPath.row)
                }
            }
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "go_to_details" {
            
            let details:DetailsLocationViewController = segue.destination as! DetailsLocationViewController
            let indexPath = self.tableView.indexPathForSelectedRow!
            let selectedLocation:UserLocation = UserLocations[indexPath.row]
            
            details.userLocation = selectedLocation
        }
    }

}
