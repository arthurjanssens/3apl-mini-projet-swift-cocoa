//
//  LocationCell.swift
//  WhereIsMyCar
//
//  Created by Arthur JANSSENS on 17/04/2017.
//  Copyright © 2017 Eleves de Supinfo. All rights reserved.
//

import UIKit

class LocationCell: UITableViewCell {
    
    @IBOutlet weak var titleCell: UILabel!
    @IBOutlet weak var commentCell: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
