//
//  ViewController.swift
//  WhereIsMyCar
//
//  Created by Arthur JANSSENS on 16/04/2017.
//  Copyright © 2017 Eleves de Supinfo. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapview: MKMapView!
    var latitude_user:Double = 0
    var longitude_user:Double = 0
    var altitude_user:Double = 0
    var center_on_user_location:Bool = true
    var reverse_map_mode:Bool = false
    var center_on_multiple_locations:Bool = false
    let locationManager = CLLocationManager()
    var targetLocation:UserLocation?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        mapview.showsUserLocation = true
        mapview.mapType = MKMapType.standard
    }

    @IBAction func unwindToHomeViewController(segue: UIStoryboardSegue) {}
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if reverse_map_mode {
            if mapview.mapType == MKMapType.standard {
                mapview.mapType = MKMapType.hybrid
            } else {
                mapview.mapType = MKMapType.standard
            }
            
            reverse_map_mode = false
        }
        
        let location = locations.last

        latitude_user = location!.coordinate.latitude
        longitude_user = location!.coordinate.longitude
        altitude_user = location!.altitude

        if(center_on_user_location){
            let the_center = location!.coordinate
            let region = MKCoordinateRegion(center: the_center, span: MKCoordinateSpan(latitudeDelta:0.1, longitudeDelta:0.1))
            self.mapview.setRegion(region, animated: true)
            center_on_user_location = false
            targetLocation = nil
        }
        
        
        if(targetLocation != nil){
            let targetLocation2D = CLLocationCoordinate2D(latitude: targetLocation!.Latitude, longitude: targetLocation!.Longitude)
            
            if(center_on_multiple_locations){
                mapview.removeAnnotations(mapview.annotations)//Pour eviter d'avoir les annotations affichées précedements
                
                let annotation = MKPointAnnotation()
                annotation.coordinate = targetLocation2D
                annotation.title = targetLocation!.Name
                annotation.subtitle = targetLocation!.Comment
                mapview.addAnnotation(annotation)
                
                //Region contenant les deux points
                var region:MKCoordinateRegion = MKCoordinateRegion()
                region.center.latitude = location!.coordinate.latitude - (location!.coordinate.latitude - targetLocation2D.latitude) * 0.5;
                region.center.longitude = location!.coordinate.longitude + (targetLocation2D.longitude - location!.coordinate.longitude) * 0.5;
                
                //Ajoute un petit padding pour eviter que les points soient collés au bords de l'écran
                region.span.latitudeDelta = fabs(location!.coordinate.latitude - targetLocation2D.latitude) * 1.4;
                region.span.longitudeDelta = fabs(targetLocation2D.longitude - location!.coordinate.longitude) * 1.4;
                mapview.setRegion(region, animated: true)
                
                center_on_multiple_locations = false
            }
        }

    }

    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("ERRORS: \(error.localizedDescription)")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier!=="go_to_save"){
            let saveVC:SaveViewController = segue.destination as! SaveViewController
            
            saveVC.userLocation = UserLocation(name: "", comment: "", altitude: altitude_user, latitude: latitude_user, longitude: longitude_user)
        }
    }

    @IBAction func center(_ sender: UIButton) {
        center_on_user_location = true
    }
    
    @IBAction func send_reverse_mode(_ sender: UIButton) {
        reverse_map_mode = true
    }
}

