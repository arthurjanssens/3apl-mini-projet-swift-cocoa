//
//  DetailsLocationViewController.swift
//  WhereIsMyCar
//
//  Created by Arthur JANSSENS on 17/04/2017.
//  Copyright © 2017 Eleves de Supinfo. All rights reserved.
//

import UIKit

class DetailsLocationViewController: UIViewController {
    
    var userLocation: UserLocation?
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var navBar: UINavigationBar!
    @IBOutlet weak var altitude: UILabel!
    @IBOutlet weak var latitude: UILabel!
    @IBOutlet weak var longitude: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navBar.topItem?.title = userLocation!.Name
        comment.text = userLocation!.Comment
        altitude.text = "Altitude: \(userLocation!.Altitude)"
        latitude.text = "Latitude: \(userLocation!.Latitude)"
        longitude.text = "Longitude: \(userLocation!.Longitude)"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier=="go_to_home"){
            let homeVC:ViewController = segue.destination as! ViewController
            homeVC.targetLocation = userLocation!
            homeVC.center_on_multiple_locations = true
            
        }else if(segue.identifier=="go_to_save"){
            let saveVC:SaveViewController = segue.destination as! SaveViewController
            
            saveVC.edit = true
            saveVC.userLocation = userLocation
        }
    }
    
    @IBAction func unwindToDetailsViewController(segue: UIStoryboardSegue) {}
    
}
