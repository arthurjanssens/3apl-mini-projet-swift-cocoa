//
//  UserLocation.swift
//  WhereIsMyCar
//
//  Created by Supinfo on 19/04/2017.
//  Copyright © 2017 Eleves de Supinfo. All rights reserved.
//

import UIKit

var UserLocations = PersistedArray<UserLocation>(archiveFileName: "SavedLocations")


class UserLocation : NSObject, NSCoding, Copyable {
    
    //MARK: Properties
    
    var Name: String = "New Location"
    var Comment: String = "A saved location"
    let Altitude: Double
    let Latitude: Double
    let Longitude: Double
    
    //MARK: Archiving Keys
    
    static let NameKey = "Name"
    static let CommentKey = "Comment"
    static let AltitudeKey = "Altitude"
    static let LatitudeKey = "Latitude"
    static let LongitudeKey = "Longitude"
    
    //MARK: Initialisation
    
    init(name: String, comment: String, altitude: Double, latitude: Double, longitude: Double) {
        Name = name
        Comment = comment
        Altitude = altitude
        Latitude = latitude
        Longitude = longitude
    }
    
    required init(copy: UserLocation) {
        Name = copy.Name
        Comment = copy.Comment
        Altitude = copy.Altitude
        Latitude = copy.Latitude
        Longitude = copy.Longitude
    }
    
    //MARK: Comparison
    
    override func isEqual(_ object: Any?) -> Bool {
        if let other = object as? UserLocation {
            return self.Altitude == other.Altitude
                && self.Latitude == other.Latitude
                && self.Longitude == other.Longitude
        } else {
            return false
        }
    }
    
    override var hash: Int {
        get {
            return Altitude.hashValue + Latitude.hashValue + Longitude.hashValue
        }
    }
    
    //MARK: NSCoding
    
    required convenience init?(coder aDecoder: NSCoder) {
        let name = aDecoder.decodeObject(forKey: UserLocation.NameKey) as? String ?? ""
        let comment = aDecoder.decodeObject(forKey: UserLocation.CommentKey) as? String ?? ""
        let altitude = aDecoder.decodeDouble(forKey: UserLocation.AltitudeKey)
        let latitude = aDecoder.decodeDouble(forKey: UserLocation.LatitudeKey)
        let longitude = aDecoder.decodeDouble(forKey: UserLocation.LongitudeKey)
        
        self.init(name: name, comment: comment, altitude: altitude, latitude: latitude, longitude: longitude)
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(Name, forKey: UserLocation.NameKey)
        aCoder.encode(Comment, forKey: UserLocation.CommentKey)
        aCoder.encode(Altitude, forKey: UserLocation.AltitudeKey)
        aCoder.encode(Latitude, forKey: UserLocation.LatitudeKey)
        aCoder.encode(Longitude, forKey: UserLocation.LongitudeKey)
    }
}
