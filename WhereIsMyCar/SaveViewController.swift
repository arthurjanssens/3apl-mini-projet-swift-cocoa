//
//  SaveViewController.swift
//  WhereIsMyCar
//
//  Created by Arthur JANSSENS on 16/04/2017.
//  Copyright © 2017 Eleves de Supinfo. All rights reserved.
//

import UIKit

class SaveViewController: UIViewController, UITextFieldDelegate {
    
    var userLocation:UserLocation?
    var edit:Bool = false
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var comment: UITextField!
    @IBOutlet weak var latitude: UILabel!
    @IBOutlet weak var longitude: UILabel!
    @IBOutlet weak var altitude: UILabel!
    @IBOutlet weak var save_button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if edit {
            name.text = userLocation!.Name;
            comment.text = userLocation!.Comment;
        }
        
        latitude.text = "Latitude: \(userLocation!.Latitude)"
        longitude.text = "Longitude: \(userLocation!.Longitude)"
        altitude.text = "Altitude : \(userLocation!.Altitude)"

        name.delegate = self
        comment.delegate = self

        updateSaveButtonState()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    //MARK: UITextFieldDelegate

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // Hide the keyboard
        textField.resignFirstResponder()
        return true
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        save_button.isEnabled = false
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        updateSaveButtonState()
    }

    //MARK: Actions

    @IBAction func save(_ sender: UIButton) {
        let location_to_save = UserLocation(name: name.text!, comment: comment.text!, altitude: userLocation!.Altitude, latitude: userLocation!.Latitude, longitude: userLocation!.Longitude)
        
        var isSaveSuccessful:Bool = false
        
        if !edit {
            isSaveSuccessful = UserLocations.append(location_to_save)
        } else {
            isSaveSuccessful = UserLocations.update(location_to_save)
        }
        
        if isSaveSuccessful {
            let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            //Go to location list
            let listVC = storyBoard.instantiateViewController(withIdentifier: "locationsList") as! LocationListViewController
            self.present(listVC, animated:true, completion:nil)
            
        } else {
            let alert = UIAlertController(title: "Error", message: "Failed to save location.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func go_to_back(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    

    //MARK: Private Methods

    private func updateSaveButtonState() {
        let name_text = name.text ?? ""
        let comment_text = comment.text ?? ""

        save_button.isEnabled = !name_text.isEmpty && !comment_text.isEmpty
    }
}
