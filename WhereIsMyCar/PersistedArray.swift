//
//  UserLocationManager.swift
//  WhereIsMyCar
//
//  Created by Thibault Lemaire on 16/04/2017.
//  Copyright © 2017 Eleves de Supinfo. All rights reserved.
//

import UIKit

protocol Copyable {
    init(copy: Self)
}

struct PersistedArray<Element> where Element : NSCoding, Element : Copyable, Element : Equatable {
    
    private var _internalArray = [Element]()
    
    private var _archivePath: String = ""
    private var _documentsDirectory: URL
    private var _archiveFileName: String
    
    var DocumentsDirectory: URL {
        get {
            return _documentsDirectory
        }
        set {
            _documentsDirectory = newValue
            updateArchiveFilePath()
        }
    }
    
    var ArchiveFileName: String {
        get {
            return _archiveFileName
        }
        set {
            _archiveFileName = newValue
            updateArchiveFilePath()
        }
    }
    
    init(archiveFileName: String = String(describing: Element.self),
         documentsDirectory: URL = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!) {
        _archiveFileName = archiveFileName
        _documentsDirectory = documentsDirectory
        updateArchiveFilePath()
        safePullElements()
    }
    
    mutating func append(_ newElement: Element) -> Bool {
        var tempArray = _internalArray
        tempArray.append(newElement)
        let isPushSuccessful = pushElements(tempArray)
        if isPushSuccessful {
            _internalArray = tempArray
        }
        return isPushSuccessful
    }
    
    mutating func remove(at index: Int) -> Element? {
        var tempArray = _internalArray
        let removedElement = tempArray.remove(at: index)
        let isPushSuccessful = pushElements(tempArray)
        if isPushSuccessful {
            _internalArray = tempArray
            return removedElement
        } else {
            return nil
        }
    }
    
    mutating func update(_ element: Element) -> Bool {
        if let correspondingIndex = _internalArray.index(where: { $0 == element }) {
            var tempArray = _internalArray
            tempArray[correspondingIndex] = element
            let isPushSuccessful = pushElements(tempArray)
            if isPushSuccessful {
                _internalArray = tempArray
            }
            return isPushSuccessful
        } else {
            return false
        }
    }
    
    public subscript(index: Int) -> Element {
        return Element(copy: _internalArray[index])
    }
    
    public var count: Int {
        get {
            return _internalArray.count
        }
    }
    
    //MARK: Peristence Helpers
    
    private mutating func updateArchiveFilePath() {
        _archivePath = _documentsDirectory.appendingPathComponent(_archiveFileName).path
    }
    
    private mutating func safePullElements() {
        if let loadedElements = pullElements() {
            _internalArray = loadedElements
        }
    }
    
    private func pullElements() -> [Element]? {
        return NSKeyedUnarchiver.unarchiveObject(withFile: _archivePath) as? [Element]
    }
    
    private func pushElements(_ pushedElements: [Element]) -> Bool {
        return NSKeyedArchiver.archiveRootObject(pushedElements, toFile: _archivePath)
    }
}
